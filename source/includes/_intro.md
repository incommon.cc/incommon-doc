# Introduction

Welcome to the IN COMMON API! You can use our API to access Commons resources in
the IN COMMON database.

This documentation shows how to use the API with `curl` from the
Shell. You can view code examples in the dark area to the right, and
you can switch the programming language of the examples with the tabs
in the top right.

This API documentation page was created with [Slate].  
The code lives in the [incommon-doc] repository on [Framagit].  
The official documentation lives at <https://doc.incommon.cc/>.  

This documentation is a collective effort of the [IN COMMON Documentors].  
You are welcome to participate in the [API documentation process].


[Slate]: https://github.com/lord/slate
[incommon-doc]: https://framagit.org/incommon.cc/incommon-doc
[Framagit]: https://framagit.org/

[IN COMMON Documentors]: https://talk.incommon.cc/groups/documentors
[API documentation process]: https://talk.incommon.cc/t/about-the-api-documentation-conference/343
