# Agents

Actors in IN COMMON API are called `Agents`. 

An agent is an **Entity** that manages a **Resource**.
An agent can be an individual but, more generally, is an organization.

Example: a university, a municipality, an association, a neighborhood council…
In Dewey Maps: equivalent to a _resource type manager_.

An agent can have one or more users who will be able to manage resources on behalf of the Agent. Agents usually match [**#welcome:transition-actors**][w:t-a] groups.

[w:t-a]: https://talk.incommon.cc/c/welcome/transition-actors
