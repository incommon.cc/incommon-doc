# Authorization

IN COMMON API uses [Pundit] to manage authorization policies.

However, with the prospect of Object Capabilities, this will be
revised in version 2 to move away from ACLs.

[Pundit]: https://github.com/varvet/pundit


## Context

The authorization context is a function of the current `User` and the current
`Agent`. Each call to the API is personal (user) but all actions depend on one
or more `Role`s of the current user in the context of the current `Agent`.

Given that an `Agent` is by design collective, different users can share a given
role within a single agent's context.

## Roles

**Roles are not cumulative but distinctive:** that is to say, one `User` may
have zero or more roles in an `Agent`, but being a `Maintainer` does not bring
the capabilities of an `Editor` nor an `Observer`.

| Role | Description
|-|-
| Observer   | Watches changes and validates against the [IN COMMON Charter]
| Editor     | Creates and edits resources for that `Agent`.
| Maintainer | Ensures the data is correct and organizes classifications
| Leader     | Grants roles to `Agent`'s members

These are the 4 authorization roles, that allow answering the following questions:

- Can the current user CREATE a resource on behalf of this `Agent`?
  - YES if the user is an Editor for that Agent. If not, the creation MAY be
    authorized within the default `Agent.incommon_agent` but in this case the
    created resources will not be visible.
- Can the current user READ the requested resource(s)? (Or what parts of the
  requested resource(s) is available for this authorization context?)
  - YES if the resource is public (visible) or if the user has a corresponding
    role in the `Agent` ; for example, a `Leader` may see the list of users in
    his `Agent`, but may not edit resources (unless he's also an `Editor`).
- Can the current user UPDATE the requested resource?
  - YES if the user is a `Maintainer`, or an `Editor` (unless the default
    `Agent` where an `Editor` can only create new resources, since they're not)
    visible.
- Can the current user DELETE the requested resource?
  - YES if the user is a `Maintainer`, or the creator of the resource.

1. An `Observer` may toggle the `visible` flag of an existing resource (e.g.,
observers of the IN COMMON Agent validating the newly created resource is
compliant with the charter).
2. An `Editor` can create new resources, or edit existing _visible_ resources. (
As such, editors of the IN COMMON Agent cannot edit even their own creations.)
3. A `Maintainer` can classify resources within the `Agent`'s taxonomy, and can
edit existing resources within its `Agent`'s scope.
4. A `Leader` can assign roles in its `Agent` to other users.

Users can have zero or more roles in a given `Agent`.

Beyond these roles, others are used elsewhere within the community, which are
not related to authorization of the API itself, but can affect contents all the
same: Documentor, Translator, etc. have an identified role but no authorization
constraints in the API. As far as the API is concerned, a `User` belongs to one
or more `Agent`s and within each has been assigned zero or more roles.

## Unauthorized Actions

The most common case when authorization fails is to reject the request with an
error. But sometimes it's better to accept the request and [process it
differently] instead.

[process it differently]: https://talk.incommon.cc/t/authorization-and-roles/391/3
