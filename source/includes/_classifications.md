# Classifications

IN COMMON API uses a 2-level classification for Resources. A Taxonomy is comprised of Categories and Sections.

You can retrieve a classification, including categories and sections in one call:

## Get the whole classification


```ruby
require 'incommon'

api = INCOMMON::API::Client.authorize!('YourAPIToken')
classification = api.taxonomies.get(taxonomy.uuid, include: [:categories, :section])
```

```shell
curl "https://api.incommon.cc/taxonomies?include=Category,Section"
  -H "Authorization: bearer YourJWT"
```

```javascript
const kittn = require('kittn');

let api = kittn.authorize('meowmeowmeow');
let kittens = api.kittens.get();
```

> The above command returns JSON structured like this:

```json
{}
```

This endpoint retrieves all taxonomies.

### HTTP Request

`GET https://api.incommon.cc/taxonomies`

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
include | none | Include Category and Section objects, when set to `include=Category,Section`
filter  | none | Filter by attribute (e.g., `filter=name`)

## Get a Specific taxonomy

```ruby
require 'incommon'

uuid = Taxonomy.find_by(uuid: 'some_uuid').pluck(:uuid).first

api = INCOMMON::API::Client.authorize!('YourAPIToken')
api.taxonomies.get(uuid, include: [:categories, :sections])
```

```shell
curl "https://api.incommon.cc/taxonomies?include=Category,Section"
  -H "Authorization: Bearer YourJWT"
```

```javascript
const Incommon = require('incommon');

let api = Incommon.authorize('YourAPIToken');
let classification = api.taxonomies.get(some_uuid);
```

> The above command returns JSON structured like this:

```json
{}
```

This endpoint retrieves a specific taxonomy.

### HTTP Request

`GET https://api.incommon.cc/taxonomies/<UUID>`

### URL Parameters

Parameter | Description
--------- | -----------
UUID      | The UUID of the taxonomy to retrieve
include   | Optionally you can add an include query parameter to retrieve Category and Section
