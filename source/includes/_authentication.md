# Authentication

IN COMMON uses API keys to allow access to the API. You can either use the
public API key or register your own API key at our [developer's
site](https://talk.incommon.cc/c/api).

IN COMMON expects that each request will include an API key or secure Token.

This token SHOULD be passed as a query string parameter, e.g.:

`https://api.incommon.cc/?api_key=23d8009f-f2ec-44b6-bcbe-58ad1d3af10b`

## Notes for v0.*

- Only query parameter authentication is implemented.
- `Authorization` header authentication MAY be implemented in future releases.

## Public, Read-only Access (GET)

There's a read-only public API key you can use to request IN COMMON public resources.
The key is **23d8009f-f2ec-44b6-bcbe-58ad1d3af10b**.

With this key you can only retrieve public resources with a GET request.

> To authorize, use this code:

```ruby
require 'incommon'

api = INCOMMON::API::Client.authorize!
```

```shell
# With shell, you can just pass the correct header with each request
curl "$api_endpoint"
  -H "Authorization: Token 23d8009f-f2ec-44b6-bcbe-58ad1d3af10b"
```

```javascript
const Incommon = require('incommon');

let api = Incommon.authorize();
```

## Authenticated Access (GET, POST, PUT, DELETE)

Authenticated access is [under way].

[under way]: https://framagit.org/incommon.cc/incommon-api/issues/8
