# Resources

Resources are the proper data objects being mapped.  
IN COMMON API knows about 4 types of resources:

| Model | API Version | Description
|-|-|-
| [Entity](#entity)   | v0 | A human individual or group, e.g., an organization, an association
| [Place](#place)     | v0 | A specific portion of territory in the world, e.g., a building, a bridge, a road, a forest
| [Service](#service) | v1 | A form of human support that can be given or exchanged
| [Thing](#thing)     | v0 | Something else, e.g., a tree, a fountain...

The Resources endpoint allows to retrieve any of such resources.  
If you want to create or otherwise act upon an existing resource, you must use its specific endpoint.

## Entity

## Place

## Service

## Thing

## GET a single resource

Each resource is identified with a `UUID`.

```ruby
require 'incommon'

uuid = "some_resource_uuid"

api = INCOMMON::API::Client.authorize!(INCOMMON.api_key)
resource = api.resources.get(uuid)
```

```shell
curl https://api.incommon.cc/resources \
     -H 'Authorization: Token 23d8009f-f2ec-44b6-bcbe-58ad1d3af10b'
```

```javascript
require 'incommon';

let api = Incommon.authorize()
let resource = api.resources.get(some_uuid)
```

## GET a specific resource type

Each resource type has its own endpoint, so if you know the type, you can do:

```ruby
require 'incommon'

uuid = "some_place_uuid"

api = INCOMMON::API::Client.authorize!(INCOMMON.api_key)
place = api.places.get(uuid)
```
## GET a specific resource without UUID

You can as well search for a resource:

```ruby
require 'incommon'

search = { name: 'ZAD NDDL' }

api = INCOMMON::API::Client.authorize!(INCOMMON.api_key)
place = api.places.find_by(search)
```
