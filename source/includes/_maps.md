# Maps

A map defines a collection of **Resources** according to a specific **Classification**. In other words, a map provides a specific view on the INCOMMON database, that is sufficient to display resources on a dynamic or static map, e.g., with Leaflet.

To simply display an existing map, calling the INCOMMON API with the Map UUID is sufficient and will return either the JSON data to feed into Leaflet, or a complete map in an `<iframe>` through a `<script>` tag.

A map is defined by its center point (a Position, serialized as `latitude` and `longitude`), the Agent responsible for this map, a Taxonomy ordering the resources, and a Collection of resources to display.

## GET a map as HTML

Status: [pending](https://framagit.org/incommon.cc/incommon-api/issues/24)

Given a map's UUID, you can simply insert this snippet of HTML in your page:

`
<script src="https://api.incommon.cc/maps/51895a0-6580-4b0a-9d78-8fcd896aec88.js?api_key=23d8009f-f2ec-44b6-bcbe-58ad1d3af10b"></script>
`

## Get a map as JSON for Leaflet

Status: [pending](https://framagit.org/incommon.cc/incommon-api/issues/25)

Using the `.json` extension explicitely returns a serialized JSON file for direct use with Leaflet.

```Shell
curl -H 'Accept: application/api+json;version=0' \
     -H 'Authorization: Token 23d8009f-f2ec-44b6-bcbe-58ad1d3af10b' \
     https://api.incommon.cc/maps/51895a0-6580-4b0a-9d78-8fcd896aec88.json
```

