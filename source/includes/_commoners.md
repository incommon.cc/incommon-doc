# Commoners

The Commoners models comprise the actual API users. As mentioned in
the [Agents](#agents) section, in ActivityPub terms, only **Agents**
are _Actors_.

| Model | API Version | Description
|-|-|-
| [Agent](#agents) | v0 | An agent is an Entity that manages a Resource
| [Role](#roles)   | v0 | 
| [Users](#users)  | v0 |

## Agents

## Roles

## Users
