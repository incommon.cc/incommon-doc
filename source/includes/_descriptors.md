# Descriptors

Descriptors are non-hierarchical classifications, available in version 1 of the API.

| Type     | API Version | Description
|-|-|-
| Event    | v2 | An event associates a **Resource** with a **Moment** in time. An event can be recurrent
| Moment   | v1 | A time descriptor (Date, duration and recurrence)
| Position | v1 | A spatial descriptor (GIS position, e.g., Point or Polygon)
| Tag      | v1 | A free-form textual descriptor


