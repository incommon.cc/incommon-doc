---
title: IN COMMON API Reference

language_tabs: # must be one of https://git.io/vQNgJ
  - shell

toc_footers:
  - <a href='https://talk.incommon.cc/c/api'>IN COMMON API discussion</a>
  - <a href='https://github.com/lord/slate'>Documentation Powered by Slate</a>

includes:
  - authentication
  - authorization
  - errors
  - agents
  - classifications
  - collections
  - descriptors
  - commoners
  - maps
  - properties
  - resources
search: true
---

# Introduction

Welcome to the IN COMMON API! You can use our API to access Commons resources in
the IN COMMON database.

This documentation shows how to use the API with `curl` from the
Shell. You can view code examples in the dark area to the right, and
you can switch the programming language of the examples with the tabs
in the top right.

This API documentation page was created with [Slate].  
The code lives in the [incommon-doc] repository on [Framagit].  
The official documentation lives at <https://doc.incommon.cc/>.  

This documentation is a collective effort of the [IN COMMON Documentors].  
You are welcome to participate in the [API documentation process].


[Slate]: https://github.com/lord/slate
[incommon-doc]: https://framagit.org/incommon.cc/incommon-doc
[Framagit]: https://framagit.org/

[IN COMMON Documentors]: https://talk.incommon.cc/groups/documentors
[API documentation process]: https://talk.incommon.cc/t/about-the-api-documentation-conference/343

# High-Level Models

These models aggregate resources from the Incommon specification.
They are used for reading from the IN COMMON API to retrieve
ready-to-use resources (which can be collections), and create
composite resources (or collections).

| Model | API Version | Description
|-|-|-
| [Agent](#agent)       | v0 | An Agent manages Resources (ActivityPub: Actor)
| [Event](#event)       | v2 | An Event associates a Resource to a Moment in time
| [Location](#location) | v0 | A Location associates a Resource to a Position in space
| [Map](#map)           | v0 | A map defines a collection of Resources according to a specific Classification

## Agent

An agent is an **Entity** that manages a **Resource**.
An agent can be an individual but, more generally, is an organization.

Example: a university, a municipality, an association, a neighborhood council...
In Dewey Maps: equivalent to _a resource type manager_. 

An agent can have one or more users who will be able to manage resources on behalf of the Agent. Agents usually match [**#welcome:transition-actors**][w:t-a] groups.

[w:t-a]: https://talk.incommon.cc/c/welcome/transition-actors

For API usage, see [Agents](#agents).

## Event

An event associates a **Resource** to a **Moment** in time.
An event can be recurrent.

For API usage, see [Events](#events).

#### Location

A location associates a **Resource** to a **Position** in space.

For API usage, see [Locations](#locations).

#### Map

A map defines a collection of **Resources** according to a specific **Classification**. In other words, a map provides a specific view on the Incommon database, that is sufficient to display resources on a dynamic or static map, e.g., with Leaflet.

To simply display an existing map, calling the Incommon API with the Map ID is sufficient and will return either the JSON data to feed into Leaflet, or a complete map in an `<iframe>` through a `<script>` tag. E.g., `<script src="https://api.incommon.cc/v1/maps/123.js"></script>`.

In Dewey Maps: equivalent to _a frame_. 

For API usage, see [Maps](#maps).
